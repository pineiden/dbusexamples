"""
Example to share a simple object of int type
"""

import sys
from PyQt5.QtCore import QObject, pyqtSignal, QSharedMemory
from PyQt5.QtWidgets import QApplication
from PyQt5.QtWidgets import QMainWindow
from PyQt5.QtWidgets import QPlainTextEdit
from PyQt5.QtWidgets import QWidget
from PyQt5.QtWidgets import QVBoxLayout
from PyQt5.QtWidgets import QPushButton
from PyQt5.QtCore import pyqtSlot, Q_CLASSINFO
from PyQt5 import QtCore, QtDBus
import simplejson as json


import struct
"""
Create and Adaptator with CLASSINFO Interface and Introspection
"""

service='csn.dbus.examples'
interface='%s.BasicTypes' %service


class IntAdaptor(QtDBus.QDBusAbstractAdaptor):
    # What produce this?????
    # send an array of bytes
    Q_CLASSINFO("D-Bus Interface", interface)
    Q_CLASSINFO("D-Bus Introspection",''
        '  <interface name="csn.dbus.examples.BasicTypes">\n'
        '	 <signal name="signal">\n'
        '	  <arg type="ai" name="data"/>\n'
        '	 </signal>\n'
        '  </interface>\n'
    '')
    signal = pyqtSignal(bytes)
    def __init__(self, parent):
        super().__init__(parent)
        self.setAutoRelaySignals(True)
        print("Int Adaptor init")

class IntInterface(QtDBus.QDBusAbstractInterface):
    signal = pyqtSignal(list)
    def __init__(self, service, path, interface, connection, parent=None):
        super().__init__(service, path, interface, connection, parent)
        print("Gather Interface init")

class GatherSignal(QObject):
    signal=pyqtSignal(list)
    def __init__(self, parent=None, *args, **kwargs):
        super().__init__(parent=parent)
        self.path="/data"
        if 'path' in kwargs.keys():
            self.path=kwargs['path']
        self.service = service
        if 'service' in kwargs.keys():
            self.service=kwargs['service']
        self.interface="%s.interface" %self.service
        self.variable = "message"
        if 'variable' in kwargs.keys():
                self.variable=kwargs['variable']
        IntAdaptor(self)
        self.connection=QtDBus.QDBusConnection.sessionBus()
        self.connection.registerObject(self.path, self)
        self.connection.registerService(self.service)
        self.iface=IntInterface(self.service, self.path, self.interface, self.connection, self)
        self.connect(self.send)
    def message(self, msg):
        if type(msg)==int:
            self.signal.emit([b for b in range(msg)])
        elif type(msg)==str:
            self.signal.emit([s for s in msg])
        else:
            self.signal.emit(msg)
    @pyqtSlot(list)
    def send(self, msg):
        assert type(msg)==list, "Mensaje no es de tipo list"
        data=msg#json.dumps(msg)
        print("send to dbus %s" %data)
        self.emit(data)
    def emit(self, data):
        msg = QtDBus.QDBusMessage.createSignal(self.path, self.interface, self.variable)
        msg << data
        print(msg)
        self.connection.send(msg)
    def connect(self, method):
        self.signal.connect(method)

class Gui(QMainWindow):
    signal=pyqtSignal(list)
    def __init__(self, parent=None, *args, **kwargs):
        super().__init__(parent)
        self.path="/data"
        if 'path' in kwargs.keys():
            self.path=kwargs['path']
        self.service = service
        if 'service' in kwargs.keys():
            self.service=kwargs['service']
        self.interface="%s.interface" %self.service
        self.variable = "message"
        if 'variable' in kwargs.keys():
            self.variable=kwargs['variable']
        self.patrol=GatherSignal(self)
        self.up_gui()
    def up_gui(self):
        main_widget=QWidget()
        layout=QVBoxLayout(main_widget)
        self.text=QPlainTextEdit()
        self.setCentralWidget(self.text)
        self.button=QPushButton('Print Hola')
        self.button.clicked.connect(self.printhola)
        layout.addWidget(self.button)
        layout.addWidget(self.text)
        self.setCentralWidget(main_widget)
        self.signal.connect(self.print_log)
        self.connect_dbus()
        self.show()
    def connect_dbus(self):
        #call adaptor for this channel
        IntAdaptor(self)
        #create connection
        self.connection=QtDBus.QDBusConnection.sessionBus()
        #get object registered
        objectReg=self.connection.objectRegisteredAt(self.path)
        print("Objeto registrado en path %s" %objectReg )
        self.connection.unregisterObject(self.path)
        regObj=self.connection.registerObject(self.path, self.interface, self)# register path, object
        regSess=self.connection.registerService(self.service)# register service
        print(self.service)
        print(self.interface)
        print("Registrado objeto %s" %regSess)
        print("Registrada session %s" %regSess)
        #call DBUS interface with my service, path, interface assigned to this object
        self.server=IntInterface(
                self.service,
                self.path,
                self.interface,
                self.connection,
                self)
        #connect dbus connection to slot
        self.connection.connect(
                self.service,
                self.path,
                self.interface,
                self.variable,
                self.update_data)#(service, path, interface, variable, slot_fn)
        if not QtDBus.QDBusConnection.sessionBus().isConnected():#check if connected
            print("Bus no conectado")
            print(QtDBus.QDBusConnection.sessionBus().lastError().message())
        print("Interface")
        print(self.server)
        print(self.server.signal)
        #connect interface message signal to slot
        self.server.signal.connect(self.update_data)
        print("Server message %s" %self.server.signal )
        #send a message
        #self.message.emit({"hola":"test message"})
    @pyqtSlot()
    def printhola(self):
        self.signal.emit(123)
        self.patrol.message(111)
    @pyqtSlot(list)    
    def print_log(self, msg):
        print(msg)
        self.text.insertPlainText(str(msg)+"\n")		
    @pyqtSlot(list)
    def update_data(self, data):
        print("Data received from dbus %s" %data)
        self.signal.emit(data)



def run_gui(data):
    #DBusQtMainLoop(set_as_default=True)
    app=QApplication(sys.argv)
    gui=Gui(**data)
    gui.showMaximized()
    sys.exit(app.exec_())


import time


class DataProcess():
    def __init__(self, *args, **kwargs):
        super().__init__()

    def run(self):
        self.signal=GatherSignal()
        count=0
        while True:
            self.signal.message(count)
            time.sleep(2)
            count+=1
            if count>=10:
                break
        count=0
        while True:
            self.signal.message("Hola Mundo")
            time.sleep(2)
            count+=1
            if count>=10:
                break
        while True:
            self.signal.message({'msg':"Hola Mundo"})
            time.sleep(2)
            count+=1
            if count>=10:
                break



import asyncio
import multiprocessing as mp
import concurrent.futures
import functools

from multiprocessing import Manager
if __name__ == "__main__":
    loop = asyncio.get_event_loop()
    workers = 2
    executor = concurrent.futures.ProcessPoolExecutor(workers)
    data = {
        "a":1,
        'service':service
    }
    dp = DataProcess(**data)
    task_gui = loop.run_in_executor(executor,
                                    functools.partial(run_gui, data))
    task_dp = loop.run_in_executor(executor,
                                   dp.run)
    tasks=[task_gui, task_dp]
    loop.run_until_complete(asyncio.gather(*tasks))
