DBUS Examples
=============


This project is create to show different examples using the channel DBus developed and designed for GNU LINUX Systems.

DBUS is an implementation to share data or information among different processes. Can be data sended by a program created with python
to other program created with c++, wathever. Disposes a common channel and the producer broadcast the data to the consumers. Works with a system called
signal-slot, in that the consumer detects when the data is sended by the producer without generate a loop to check new data. Is  a big improvement to
the creation of software, so is important to learn how it works and how handle different types of data.

The main system is developed by [freedesktop.org](https://es.wikipedia.org/wiki/Freedesktop.org) foundation, and has the principal library written  in C language.
There are also a lot of libraries for different languages that connect to the DBUS daemon.

To know more about DBUS check the [homepage](https://www.freedesktop.org/wiki/Software/dbus/)

Install dependences
==================

## Install Libraries DBUS and QT5

Using the repositories and the installer for QT

## Python's dependences

To run python's examples you have to install the requeriments files using pip

`pip install -r requeriments.txt`


DBus Monitoring
===============

To check what channels there are running on DBUS and what data is shared, you can use this programs:

* dbus-monitor
* qdbusviewer
* d-free

Libraries to connect DBUS 
========================

## Python

* PyQT5
* dbus-python 
* glib 

## C

## C++

## Ada

## Go

## Rust

## Bash

## Perl


Comparative Types's Table
========================

[General Reference](https://dbus.freedesktop.org/doc/dbus-specification.html#container-types)

| DBus        | Interface ASCII code                           | Desc                                                                                        | C | Python | Qt |
|-------------|------------------------------------------------|---------------------------------------------------------------------------------------------|---|--------|----|
| BYTE        | y  (121)                                       | Unsigned 8bit integer                                                                       | byte | bytes  | qbytestring |
| BOOLEAN     | b (98)                                         | Boolean value, 0 false, 1 true                                                              |   |        |    |
| INT16       | n (110)                                        | Signed 16bit integer                                                                        |   |        |    |
| UINT16      | q (113)                                        | Unsigned 16bit integer                                                                      |   |        |    |
| INT32       | i (105)                                        | Signed 32bit integer                                                                        |   |        |    |
| UINT32      | u (117)                                        | Unsigned 32bit integer                                                                      |   |        |    |
| INT64       | x (120)                                        | signed 64 bit integer                                                                       |   |        |    |
| UINT64      | t (116)                                        | Unsigned 64 bit integer                                                                     |   |        |    |
| DOUBLE      | d (100)                                        | IEEE 745 double precision floating point                                                    |   |        |    |
| UNIX_FD     | h (104)                                        | Unsigned 32 bit integer representing an index into an aout of band array of file descriptor |   |        |    |
| STRING      | s (115)                                        | No extra constrains                                                                         |   |        |    |
| OBJECT_PATH | o (111)                                        | Must be a syntactically object path                                                         |   |        |    |
| SIGNATURE   | h (103)                                        | Zero or more single complete tpes                                                           |   |        |    |
| STRUCT      | (ii)                                           | Strcut with 2 integers                                                                      |   |        |    |
| STRUCT      | (i(ii))                                        | Struct with integer and other struct                                                        |   |        |    |
| ARRAY       | ai                                             | Array of type int                                                                           |   |        |    |
| ARRAY       | a(ii)                                          | Array of some structure with 2 integers                                                     |   |        |    |
| ARRAY       | aai                                            | Array of array of integers                                                                  |   |        |    |
| VARIANT     | v                                              | Variable tpye                                                                               |   |        |    |
| DICT_ENTRY  | e (ASSCI 'e'),123 (ASCII '{'), 125 (ASCII '}') | Dictionary entry                                                                            |   |        |    |




Examples Here
============

## With PyQt

### dbus int 

### dbus float

### dbus str

### dbus list

### dbus dict

## With PyDBus

### dbus int 

### dbus float

### dbus str

### dbus list

### dbus dict
